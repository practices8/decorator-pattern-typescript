export abstract class Bevarage {
    public abstract getDescription(): string;
    public abstract getCost(): number;
}